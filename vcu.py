#!/usr/bin/python
#--------------------------------------
#    ___  ___  _ ____          
#   / _ \/ _ \(_) __/__  __ __ 
#  / , _/ ___/ /\ \/ _ \/ // / 
# /_/|_/_/  /_/___/ .__/\_, /  
#                /_/   /___/   
#
#  RPiSpy Video Capture Unit
#
# A simple script to create a basic 
# video logger that records a define 
# number of video segments in a 
# continuous loop. It requires a basic
# hardware configuration.
#
# Author : Matt Hawkins
# Date   : 19/12/2014
#
# http://www.raspberrypi-spy.co.uk/
#
# Uses the picamera Python library
# http://picamera.readthedocs.org/
#
#--------------------------------------

# Import standard libraries
import os
import time
import datetime

# Import third party libraries
import RPi.GPIO as GPIO
import picamera

# Import my libraries
import config

#================================================
# Define Functions
#================================================
def GetVideoCount(path):
  # Counts existing video files
  video_count = 0
  for root,dirs,files in os.walk(path) :
    for file in files:
      if file[-5:]=='.h264':
        video_count = video_count + 1
  return video_count
  
def GetFileName():
  # Generates a filename with timestamp 
  filename = time.strftime("%Y%m%d_%H%M%S", time.gmtime())
  return filename
  
def DeleteOldest(path):
  # Deletes oldest file in a specified directory
  current = os.getcwd()
  os.chdir(path)
  files = sorted(os.listdir(path), key=os.path.getmtime)
  oldest = files[0]
  newest = files[-1]
  os.chdir(current)
  filename, extension = os.path.splitext(oldest)
  
  base = os.path.join(config.VIDEO_PATH,filename)
  if os.path.exists(base + '.jpg'):
    os.remove(base + '.jpg')
    
  if os.path.exists(base + '.h264'):    
    os.remove(base + '.h264')  
    
  if os.path.exists(base + '.mp4'):    
    os.remove(base + '.mp4')  
  
  print "  Deleted " + base
   
def SetupGPIO():
  # Tell GPIO library to use GPIO references
  GPIO.setmode(GPIO.BCM)
  GPIO.setwarnings(False)
  # Set up the GPIO pins as outputs and set False  
  GPIO.setup(config.POWERLED, GPIO.OUT)    
  GPIO.setup(config.ACTIVLED, GPIO.OUT)
  GPIO.setup(config.BUZZERGPIO, GPIO.OUT)    
  GPIO.setup(config.BUTTONGPIO, GPIO.IN)
     
def TidyGPIO():
  # Turns off LEDs
  LEDS(config.POWERLED,False)
  LEDS(config.ACTIVLED,False)  
    
def LEDS(led,state):
  # Turns on/off LEDs
  GPIO.output(led, state)      
   
def BUZZER(secs):
  # Buzz for a number of secs
  GPIO.output(config.BUZZERGPIO, True)   
  time.sleep(secs)
  GPIO.output(config.BUZZERGPIO, False)     
   
def WriteLog(line):
  f = open(os.path.join(config.LOG_PATH,'log.txt'), 'a')
  stamp = time.strftime("[%H:%M:%S %Y-%m-%d] ", time.gmtime())  
  f.write(stamp + line + '\n')
  f.close()

#================================================
# End of function definitions
#================================================

print "RPiSpy VCU"

# Setup variables
KeepGoing     = True
ButtonCounter = 0

# Setup GPIO pins
SetupGPIO()

LEDS(config.POWERLED,True)

BUZZER(0.25) 

# Create camera object and setup
camera = picamera.PiCamera()
camera.framerate  = config.VIDEO_FRAMERATE
camera.resolution = config.VIDEO_RESOLUTION
camera.led = config.VIDEO_LED
camera.vflip = config.VIDEO_VFLIP
camera.rotation = config.VIDEO_ROTATE

WriteLog('Start')
print "Camera object created"

# Check state of button
if GPIO.input(config.BUTTONGPIO)==1:
  KeepGoing=False
  config.AUTO_SHUTDOWN=False
  WriteLog('Button pressed - Abort')   
  print "Button pressed - Abort"
else:
  BUZZER(0.25) 

# Main loop
while KeepGoing==True:
 
  # Count existing video files
  while GetVideoCount(config.VIDEO_PATH) >= config.VIDEO_COUNT:
    # Maximum number of files so delete oldest
    WriteLog('Delete Oldest')
    DeleteOldest(config.VIDEO_PATH)

  filename = GetFileName()
  start_time = time.time()
  recording_time = 0

  # Capture image to use as thumbnail
  camera.capture(os.path.join(config.VIDEO_PATH,filename + '.jpg'), use_video_port=True)
  
  # Start recording
  stamp = datetime.datetime.fromtimestamp(start_time).strftime('%H:%M:%S') 
  WriteLog('Start recording : ' + stamp + ' : ' + filename + '.h264')  
  print 'Start recording : ' + stamp + ' : ' + filename + '.h264'  
  camera.start_recording(os.path.join(config.VIDEO_PATH,filename + '.h264'))
  
  # Wait loop
  while (recording_time<config.VIDEO_INTERVAL) and (KeepGoing==True):
    
    GPIO.output(config.ACTIVLED, not GPIO.input(config.ACTIVLED))
    
    # Wait 2 seconds
    camera.wait_recording(2)
      
    # Check state of button
    if GPIO.input(config.BUTTONGPIO)==1:
      ButtonCounter += 1
      LEDS(config.POWERLED,False)
      BUZZER(0.25)       
      if ButtonCounter>2:
        # Button pressed for at least 4 seconds
        KeepGoing = False
        LEDS(config.POWERLED,True)  
      print "Button pressed x " + str(ButtonCounter)        
    else:
      ButtonCounter = 0
      LEDS(config.POWERLED,True)       

    # Get current recording time (secs)
    recording_time = time.time() - start_time    

  camera.stop_recording()
  WriteLog('Stop recording') 
  print "  Stop recording"
  
camera.close()
TidyGPIO()

# Shutdown the system
if config.AUTO_SHUTDOWN==True:
  WriteLog('Shutdown')
  print "Shutdown!"
  BUZZER(1)   
  os.system('shutdown -h now') 
else:
  WriteLog('Exit')  
  print "Exit!" 