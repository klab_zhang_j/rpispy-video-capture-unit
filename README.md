___  ___  _ ____          
       / _ \/ _ \(_) __/__  __ __ 
      / , _/ ___/ /\ \/ _ \/ // / 
     /_/|_/_/  /_/___/ .__/\_, /  
                    /_/   /___/   

# RPiSpy Video Capture Unit
The RPiSpy Video Capture Unit is a basic script to record
video files in a loop. The user can define the total number
of files to keep and how long each file should last.

# Hardware
The script was designed to use a basic hardware/software configuration:

* Raspberry Pi
* Pi camera module
* 8GB SD card (16GB recommended)
* Standard Raspbian image
* 1 button attached to GPIO7
* 1 LED and resistor attached to GPIO4
* 1 LED and resistor attached to GPIO11

In my test unit I used a BerryClip addon board to give me
quick access to two LEDs, a switch and a buzzer.

# Files
* config.py - configuration settings
* cron.py - file called on boot
* instructions.txt
* prepare_mp4.bat
* prepare_mp4.sh - convert all h264 files to MP4
* vcu.py - main Python script

#### cron.py
This script is run when the Pi boots. It looks for a network connection
and only runs vcu.py if it doesn't find one. This allows the Pi to boot
normally when you have connected it to your network to copy video files.

#### vcu.py
This is the main Python script which does most of the work. It is called
from cron.py when the Pi boots.

#### config.py
This file contain the settings used by vcu.py. It allows the user to adjust
various parameters. If "/boot/vcu_config.py" exists it is copied over the config file.
The vcu_config.py can be used to edit settings on a Windows PC which can see the /boot/
partitiion on the SD card.

#### prepare_mp4.sh
This script provides an easy way to convert the recorded h264 files in the
"/home/pi/rpispy_vcu/videos/" directory to MP4 files. It may take a while to convert depending 
on the total duration of the videos. It uses MP4Box.

# Picamera Python Library
To start and stop video recording this script use the
picamera Python library :
http://picamera.readthedocs.org/

# Additional information
Visit my site for more information :
[http://www.raspberrypi-spy.co.uk/2014/11/how-to-create-a-raspberry-pi-video-capture-unit-part-1/](http://www.raspberrypi-spy.co.uk/2014/11/how-to-create-a-raspberry-pi-video-capture-unit-part-1/)